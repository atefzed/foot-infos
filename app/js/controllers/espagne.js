function LeagueEspagneCtrl($scope, InfoFootService) {

  $scope.title = 'League d\'espagne';
  $scope.feeds = [];

  InfoFootService.getEspagneInfo().then(function (data) {
  	$scope.feeds = data.entries;
  	$scope.$apply();
  }, function(err) {
    console.log(err);
  });

}

LeagueEspagneCtrl.$inject = ['$scope', 'InfoFootService'];

export default {
  name: 'LeagueEspagneCtrl',
  fn: LeagueEspagneCtrl
};
