function DetailCtrl($scope, $routeParams, InfoFootService) {

  let feedId = $routeParams.id;

  $scope.feed = {};

	InfoFootService.getInfo().then(function (data) {
  	$scope.feed = data.entries.find(function (entrie) {
  		return entrie.guid == feedId;
  	});
  	$scope.$apply();
  }, function(err) {
    console.log(err);
  });

}

DetailCtrl.$inject = ['$scope', '$routeParams', 'InfoFootService'];

export default {
  name: 'DetailCtrl',
  fn: DetailCtrl
};
