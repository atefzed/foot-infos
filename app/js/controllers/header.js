function HeaderCtrl() {

  // ViewModel
  const vm = this;

  vm.getClass = function (path) {
	  return (window.location.pathname === path) ? 'active' : '';
	}
}

export default {
  name: 'HeaderCtrl',
  fn: HeaderCtrl
};
