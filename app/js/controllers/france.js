function LeagueFranceCtrl($scope, InfoFootService) {

  // ViewModel
  // let vm = this;
  $scope.title = 'League de france';
  $scope.feeds = [];

  InfoFootService.getFranceInfo().then(function (data) {
  	$scope.feeds = data.entries;
    $scope.$apply();
  }, function(err) {
    console.log(err);
  });

}

LeagueFranceCtrl.$inject = ['$scope', 'InfoFootService'];

export default {
  name: 'LeagueFranceCtrl',
  fn: LeagueFranceCtrl
};
