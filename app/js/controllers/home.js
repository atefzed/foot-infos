function HomeCtrl($scope, InfoFootService) {

  $scope.feeds = [];


	InfoFootService.getInfo().then(function (data) {
  	$scope.feeds = data.entries;
  	$scope.$apply();
  }, function(err) {
    console.log(err);
  });
  

}

HomeCtrl.$inject = ['$scope', 'InfoFootService'];

export default {
  name: 'HomeCtrl',
  fn: HomeCtrl
};
