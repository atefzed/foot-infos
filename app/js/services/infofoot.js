function InfoFootService() {
  'ngInject';

  const service = {};

  service.getInfo = function() {
    return new Promise(function(resolve, reject) {
      feednami.load('http://www.matchendirect.fr/rss/info.xml',function(result){
        if(result.error){
          reject(result.error);
        } else {
          resolve(result.feed);
        }
      });
    });
  };

  service.getFranceInfo = function() {
    return new Promise(function(resolve, reject) {
      feednami.load('http://www.matchendirect.fr/rss/foot-ligue-1-c16.xml',function(result){
        if(result.error){
          reject(result.error);
        } else {
          resolve(result.feed);
        }
      });
    });
  };

  service.getEspagneInfo = function() {
    return new Promise(function(resolve, reject) {
      feednami.load('http://www.matchendirect.fr/rss/foot-ligue-1-c176.xml',function(result){
        if(result.error){
          reject(result.error);
        } else {
          resolve(result.feed);
        }
      });
    });
  };

  return service;

}

export default {
  name: 'InfoFootService',
  fn: InfoFootService
};
