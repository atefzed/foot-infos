function OnConfig($routeProvider, $locationProvider) {
  'ngInject';

  $locationProvider.html5Mode(true);

  $routeProvider.
    when('/', {
        templateUrl: 'home.html',
        controller: 'HomeCtrl'
    }).
    when('/france', {
        templateUrl: 'france.html',
        controller: 'LeagueFranceCtrl'
    }).
    when('/espagne', {
        templateUrl: 'espagne.html',
        controller: 'LeagueEspagneCtrl'
    }).
    when('/detail/:id', {
        templateUrl: 'detail.html',
        controller: 'DetailCtrl'
    }).
    otherwise({
        redirectTo: '/'
    });

}

export default OnConfig;
